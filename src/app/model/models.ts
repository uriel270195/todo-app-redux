export class TodoM {
    public id: number;
    public completado: boolean;
    constructor(public texto: string) {
        this.texto = texto.charAt(0).toLocaleUpperCase() + texto.slice(1);
        this.id = Math.random();
    }
}
