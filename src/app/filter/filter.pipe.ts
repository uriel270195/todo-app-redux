import { Pipe, PipeTransform } from '@angular/core';
import { TodoM } from '../model/models';
import { filtrosValidos } from './filter.actions';

@Pipe({
  name: 'filterTodo'
})
export class FilterPipe implements PipeTransform {

  transform(todos: TodoM[], filtro: filtrosValidos): TodoM[] {
    switch (filtro) {
      case 'completados':
        return todos.filter(f => f.completado);
      case 'pendientes':
        return todos.filter(f => !f.completado);
      default:
        return todos
    }
  }

}
