import { Component, OnInit } from '@angular/core';
import { filtrosValidos, SetFiltroAction } from 'src/app/filter/filter.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducers';
import { TodoM } from 'src/app/model/models';
import { BorrarAllTodoAction } from '../todo.actions';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styles: []
})
export class TodoFooterComponent implements OnInit {
  filtrosValidos: filtrosValidos[] = ['todos', 'completados', 'pendientes'];
  filtroActual:filtrosValidos;
  pendientes: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.subscribe( state => {
      this.filtroActual = state.filtro;
      this.contarpendientes(state.todos);
    });
  }

  cambiarFiltro(filtro: filtrosValidos) {
    const accion = new SetFiltroAction(filtro);
    this.store.dispatch(accion);
  }

  contarpendientes(todos: TodoM[]): void{
    this.pendientes = todos.filter(f => !f.completado ).length;
  }

  borrarCompleatdos() {
    const action = new BorrarAllTodoAction();
    this.store.dispatch(action);
  }

}
