import { Component, OnInit, Input, ElementRef, ViewChildren, ViewChild } from '@angular/core';
import { TodoM } from 'src/app/model/models';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducers';
import { ToggleTodoAction, EditarTodoAction, BorrarTodoAction } from '../todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styles: []
})
export class TodoItemComponent implements OnInit {
  @Input()todo: TodoM;
  @ViewChild('txtInputFisico', {static: false}) txtInputFisico: ElementRef;
  checkField : FormControl;
  txtinput: FormControl;
  editando: boolean;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.checkField = new FormControl(this.todo.completado);
    this.txtinput = new FormControl(this.todo.texto, Validators.required);
    this.checkField.valueChanges.subscribe(() => {
      const accion = new ToggleTodoAction(this.todo.id);
      this.store.dispatch(accion);
    });
  }

  editar(){
    this.editando = true;
    setTimeout(() => {
      this.txtInputFisico.nativeElement.select();
    }, 0);
  }

  blur(){
    this.editando = !this.editando
    if (this.txtinput.invalid) {
      return;
    }

    if (this.txtinput.value === this.todo.texto){
      return;
    }
    const editado = new EditarTodoAction(this.txtinput.value, this.todo.id);
    this.store.dispatch(editado);
  }
  borrar() {
    const borrarTodo = new BorrarTodoAction(this.todo.id);
    this.store.dispatch(borrarTodo);
  }

}
