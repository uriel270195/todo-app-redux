import { Acciones, AGREGAR_TODO, TOGGLE_TODO, EDITAR_TODO, BORRAR_TODO, TOGGLE_ALL_TODO, BORRAR_ALL_TODO } from './todo.actions';
import { TodoM } from '../model/models';

const todo1 = new TodoM('Estudiar angular');
const todo2 = new TodoM('Estudiar mobile');
const todo3 = new TodoM('Estudiar back');
todo2.completado = true;
const estadoInicial: TodoM[] = [todo1, todo2, todo3];

export function todoReducer(state = estadoInicial, action: Acciones): TodoM[] {
    switch (action.type) {
        case AGREGAR_TODO:
            const todo = new TodoM(action.texto);
            // cloando un estado actual
            return [...state, todo];
        case TOGGLE_TODO:
            // cloando un estado actual
            return state.map(m => {
                if (m.id === action.id) {
                    return {
                        ...m,
                        completado: !m.completado
                    }
                }
                return m;
            });
        case EDITAR_TODO:
            // cloando un estado actual
            return state.map(m => {
                if (m.id === action.id) {
                    return {
                        ...m,
                        texto: action.texto
                    };
                }
                return m;
            });
        case BORRAR_TODO:
            // cloando un estado actual
            return state.filter(f => {
                if (f.id !== action.id) {
                    return f;
                }
            });
        case TOGGLE_ALL_TODO:
            // cloando un estado actual
            return state.map(f => ({ ...f, completado: action.completado }));
        case BORRAR_ALL_TODO:
            // cloando un estado actual
            return state.filter(f => !f.completado);
        default:
            return state;
    }
}
